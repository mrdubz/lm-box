$(document).ready(function () {

    //Index slider
    $('.js-index-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        infinite: true,
        fade: true,
        swipeToSlide: true,
        prevArrow: '.js-index-prev',
        nextArrow: '.js-index-next'
    });

    //Item slider
    $('.js-item-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        infinite: true,
        fade: true,
        prevArrow: '.item-images__prev',
        nextArrow: '.item-images__next'
    });

    //Item slider
    $('.js-scheme-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        infinite: true,
        fade: false,
        prevArrow: '.js-scheme-prev',
        nextArrow: '.js-scheme-next'
    });

    //Lightbox
    lightbox.option({
        'disableScrolling': true
    });

    //Selects
    $('.select-custom').select2({
        minimumResultsForSearch: -1
    });

    //Footer nav
    $('.js-show-footernav').click(function () {
       $('.app-wrap').toggleClass('show-nav');
    });
    $('.js-hidefooter').click(function () {
        $('.app-wrap').removeClass('show-nav');
    });

});